<?php 
	require_once('Animal.php');
	require_once('Frog.php');
	require_once('Ape.php');

	$sheep = new Animal("shaun");
	echo "Nama hewan : $sheep->name <br>"; // "shaun"
	echo "Jumlah kaki : $sheep->legs <br>"; // 2
	echo "Hewan berdarah dingin : $sheep->cold_blooded <br>"; // false

	echo "<br>";

	$sungokong = new Ape("kera sakti");
	echo "Nama hewan : $sungokong->name <br>"; // "sungokong"
	echo "Jumlah kaki : $sungokong->legs <br>"; // 2
	echo "Hewan berdarah dingin : $sungokong->cold_blooded <br>"; // false
	$sungokong->yell(); // "Auooo"

	echo "<br><br>";

	// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
	$kodok = new Frog("buduk");
	echo "Nama hewan : $kodok->name <br>"; // "kodok"
	echo "Jumlah kaki : $kodok->legs <br>"; // 4
	echo "Hewan berdarah dingin : $kodok->cold_blooded <br>"; // false
	$kodok->jump(); // "hop hop"
?>